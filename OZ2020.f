c
      implicit integer*4(i-n),real*8(a-h,o-z)
      common/sys/rho,phi,x(2),diam(3)
      common/par/alfa,rmax,ez,nrho,nr,iclos
      open(10,file='parameters_05.dat',status='unknown')
      open(11,file='gr_05.dat',status='unknown')
      open(12,file='cr_05.dat',status='unknown')
      open(13,file='cq_05.dat',status='unknown')
      open(14,file='fq_05.dat',status='unknown')
      open(15,file='df_05.dat',status='unknown')
	  open(16,file='sq_05.dat',status='unknown')
      open(17,file='hq_05.dat',status='unknown')
      call input
      call oz
      close(10)
      close(11)
      close(12)
      close(13)
      close(14)
      close(15)
	  close(16)
      close(17)
      stop
      end
c 
      subroutine input
      implicit integer*4(i-n),real*8(a-h,o-z)
      common/sys/rho,phi,x(2),diam(3)
      common/par/alfa,rmax,ez,nrho,nr,iclos
      pi=4.d0*datan(1.d0) 
c If iclos=1 then PY. If iclos=2 then Kinoshita.       
      iclos=2
      fi1=0.0250
c      write(*,*)'volume fraction 2: '
c      read(*,*)fi2
       fi2=0.050-fi1
       phi=fi1+fi2
      diam(1)=1.d0
      diam(3)=1.d0/5.d0
      alfa=1.d0
      diam(2)=(diam(1)+diam(3))/2.d0
      x(1)=1.d0/(1.d0+(fi2/fi1)*(diam(1)/diam(3))**3)
      x(2)=1.d0-x(1)
      fi=fi1+fi2
      rho=6.d0*fi/(pi*(x(1)*diam(1)**3+x(2)*diam(3)**3))
	  delta=0.d0
c Asakura-Oosawa Mixture
c      diam(3)=0.d0
c	delta=-0.0375
c	print*,'Non-additive route: [1] or [2]'
c	read(*,*)nrou
c	if (nrou .eq. 1) then
c	   diam(3)=(diam(3)-delta*diam(1))/(1.+delta)
c	else
c	   diam(2)=diam(2)*(1.+delta)
c	endif
c	print*,'Sigma_s: ',diam(3)
      rmax=10.d0*diam(1)
      nr=2**13
      nrho=2000
      ez=1.d-3
      return
      end
c 
      subroutine oz
      implicit integer*4(i-n),real*8(a-h,o-z)
      parameter (nm=2**17)
      dimension r(nm),g(nm,3),c(nm,3),gam(nm,3),gam0(nm,3),b(nm,3)
      dimension h(nm,3)
      dimension gam1(nm,3),r1(nm),dif(2),gcont(3),m(3),c2(nm,3)
      dimension ceff(nm),gameff(nm),beff(nm),a0(3),a1(3),a2(3)
	dimension s(nm,3)
	dimension beff_py(nm),beff_mar(nm),b_py(nm,3),b_mar(nm,3)
      common/oz2c/q(nm),dr,kj
      common/sys/rho,phi,x(2),diam(3)
      common/par/alfa,rmax,ez,nrho,nr,iclos      
      pi=4.d0*datan(1.d0)
      do k=1,3
         do i=1,nm
            gam0(i,k)=0.d0
         enddo
      enddo
      rhoa=rho
      drho=rho/nrho
      dq=pi/rmax
      dr=rmax/nr
      do i=1,nr
         r(i)=(i-1)*dr
         q(i)=(i-1)*dq
      enddo
      kj=1
      rho=kj*drho
5     call ng(nr,gam0,gam,c,r)
      if (kj.gt.1) goto 10
      do k=1,3
         do i=1,nr
            gam0(i,k)=gam(i,k)
         enddo
      enddo
      kj=kj+1
      rho=kj*drho
      goto 5
10    call extrap(gam0,gam,nr,rho,drho)
15    do k=1,3
         do i=1,nr
            gam1(i,k)=gam(i,k)
         enddo
      enddo
      kj=kj+1
      rho=kj*drho
      call ng(nr,gam0,gam,c,r)
      if (kj.eq.nrho) goto 20
      call extrap(gam1,gam,nr,rho,drho)
      do k=1,3
         do i=1,nr
            gam0(i,k)=gam1(i,k)
         enddo
      enddo
      goto 15
20    kj=nrho
      do k=1,3
         do i=1,nr
            if (r(i).lt.diam(k)) then
               m(k)=i
            endif
         enddo
      enddo
      do k=1,3
         g(1,k)=0.d0
         h(1,k)=g(1,k)-1.d0
         do i=2,nr
            g(i,k)=gam(i,k)+c(i,k)+1.d0
            h(i,k)=g(i,k)-1.d0
         enddo
      enddo
c      do k=1,3
c         mk=m(k)
c         a=(gam(mk+1,k)-gam(mk,k))/dr
c         b=gam(mk,k)-a*r(mk)
c         gamcont=a*diam(k)+b
c         if (iclos.eq.1) then
c            gcont(k)=gamcont+1.d0
c         else           
c            gcont(k)=(dexp(gamcont*f)-1.d0)/f+1.d0
c         endif
c      enddo
c      do i=1,nr
c         r1(i)=x(1)**2*c(i,1)+2.d0*x(1)*x(2)*c(i,2)+x(2)**2*c(i,3)
c         r1(i)=r1(i)*r(i)**2
c      enddo
c      chic=0.d0
c      do i=2,nr-1
c         chic=chic+r1(i)
c      enddo
c      chic=dr*(chic+(r1(1)+r1(nr))/2.d0)
c      chic=1.d0-4.d0*pi*rho*chic
c      pv=x(1)**2*diam(1)**3*gcont(1)
c      pv=pv+2.d0*x(1)*x(2)*diam(2)**3*gcont(2)
c      pv=pv+x(2)**2*diam(3)**3*gcont(3)
c      pv=rho*(1.d0+2.d0*pi*rho*pv/3.d0)
c      pexv=pv/rho-1.d0
c      write(10,*)'  '
c      write(10,*)'chic=',sngl(chic)
c      write(10,*)'pexv=',sngl(pexv)
c      write(10,*)'g11(sigma1+)=',sngl(gcont(1))
c      write(10,*)'g12(((sigma1+sigma2)/2)+)=',sngl(gcont(2))
c      write(10,*)'g22(sigma2+)=',sngl(gcont(3))
      do i=1,nr
         if (r(i).le.4.d0*diam(1)) then
            write(11,100)r(i),g(i,1),g(i,2),g(i,3)
         endif
         if (r(i).le.4.d0*diam(1)) then
            write(12,100)r(i),c(i,1),c(i,2),c(i,3)
	      c2(i,1)=c(i,1)
         endif
      enddo
c Depletion forces
      call count(c,r,nr,a0,a1,a2)
      call fftm(c,rmax,nr,1)
      call discount(c,q,nr,a0,a1,a2)
      do i=1,nr
         ceff(i)=1.d0-rho*x(2)*c(i,3)
         ceff(i)=c(i,1)+rho*x(2)*c(i,2)**2/ceff(i)
         gameff(i)=rho*x(1)*ceff(i)**2
         gameff(i)=gameff(i)/(1.d0-rho*x(1)*ceff(i))
      enddo

c Escribe las c's y las h's en el espacio de Fourier
      call count(h,r,nr,a0,a1,a2)
      call fftm(h,rmax,nr,1)
      call discount(h,q,nr,a0,a1,a2)
      do i=1,nr
         if (i .gt. 1) then
            write(13,120)q(i),c(i,1),c(i,2),c(i,3)
            write(17,120)q(i),h(i,1),h(i,2),h(i,3)
        endif
      enddo


c Partial structure factors

	do i=1,nr
         delta=(1.d0-rho*x(1)*c(i,1))*(1.d0-rho*x(2)*c(i,3))
         delta=delta-rho**2*x(1)*x(2)*c(i,2)**2
         s(i,1)=(1.d0-rho*x(2)*c(i,3))*c(i,1)
         s(i,1)=(s(i,1)+rho*x(2)*c(i,2)**2)/delta
         s(i,2)=c(i,2)/delta
         s(i,3)=(1.d0-rho*x(1)*c(i,1))*c(i,3)
         s(i,3)=(s(i,3)+rho*x(1)*c(i,2)**2)/delta
         s(i,1)=x(1)+rho*x(1)**2*s(i,1)
         s(i,2)=rho*x(1)*x(2)*s(i,2)
         s(i,3)=x(2)+rho*x(2)**2*s(i,3)
	   snn=s(i,1)+2*s(i,2)+s(i,3)
c Formato de Edilio
         qed=q(i)/0.15d0
         s11ed=s(i,1)/x(1)
         s12ed=s(i,2)/dsqrt(x(1)*x(2))
         s22ed=s(i,3)/x(2)         
c	   if (i .gt. 1) write(16,120)q(i)/(0.15),s(i,1),s(i,2),s(i,3),snn
       if (i .gt. 1) write(16,120)qed,s11ed,s12ed,s22ed
	open(22,file="inversa-snn.dat",status="unknown")
	   snn_inversa=1./snn
	write(22,*)sngl(q(i)),sngl(snn_inversa)
      enddo
c	print*,'k_min: '
c	read(*,*)dkmin
c	call glass(s,dkmin)


c Se controla la espinodal
	open(13,file="scc.dat",status="unknown")
	write(13,*)' '
	write(13,*)'Entre mas cercanos a 0 sean estos valores,'
	write(13,*)'mas cerca estaremos de la curva espinodal.'
	write(13,*)'q             x1x2/Scc(q)'
      do i=1,50
	   delta=x(1)**2*c(i,1)+2.d0*x(1)*x(2)*c(i,2)
	   delta=delta+x(2)**2*c(i,3)
	   delta=1.d0-rho*delta
	   scc=(1.d0-rho*x(1)*c(i,1))*(1.d0-rho*x(2)*c(i,3))
	   scc=scc-rho**2*x(1)*x(2)*c(i,2)**2
         scc=scc/delta
	write(13,*)sngl(q(i)),sngl(scc)
	enddo
c Sigue el potencial efectivo!	
      call fft(gameff,rmax,nr,2)
      do i=1,nr
         ceff(i)=g(i,1)-1.d0-gameff(i)

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc FUNCION PUENTE Kinoshita
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c Kinoshina-like beff(r)
	open(31,file="beff.dat",status="unknown")
         if (r(i).gt.diam(1)) then
	      if (gameff(i) .gt. 0.d0) then
	         beff(i)=-0.5*(gameff(i)**2.)/(1.+0.8*gameff(i))
	      else
	         beff(i)=-0.5*(gameff(i)**2.)/(1.-0.8*gameff(i))
	      endif
c	   write(31,100)r(i)-diam(1),ceff(i),gameff(i),beff(i)
         endif
      enddo
c Kinoshina b(r)
	do i=1,nr
	   if (r(i).gt.diam(1)) then
	      if (gam(i,1) .gt. 0.d0) then
	         b(i,1)=-0.5*(gam(i,1)**2)/(1.+0.8*gam(i,1))
	      else
	         b(i,1)=-0.5*(gam(i,1)**2)/(1.-0.8*gam(i,1))
	      endif
	   endif
	   write(31,120)r(i)-diam(1),beff(i),b(i,1)-beff(i),
     c			beff(i)-b(i,1),b(i,1)
	enddo
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc FUNCION PUENTE PY
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	open(35,file="beff_py.dat",status="unknown")
	do i=1,nr
         if (r(i).gt.diam(1)) then
	         beff_py(i)=log(gameff(i) + 1.)-gameff(i)
	      endif
c	   write(31,100)r(i)-diam(1),ceff(i),gameff(i),beff(i)
      enddo
c
	do i=1,nr
	   if (r(i).gt.diam(1)) then
	         b_py(i,1)=log(gam(i,1)+1.)-gam(i,1)
	      endif
	   write(35,120)r(i)-diam(1),beff_py(i),b_py(i,1)-beff_py(i),
     c			beff_py(i)-b_py(i,1),b_py(i,1)
	enddo
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc FUNCION PUENTE MARIANO
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	open(36,file="beff_mar.dat",status="unknown")
	do i=1,nr
         if (r(i).gt.diam(1)) then
	         beff_mar(i)=log(g(i,1))-gameff(i)
	      endif
c	   write(31,100)r(i)-diam(1),ceff(i),gameff(i),beff(i)
      enddo
c
	do i=1,nr
	   if (r(i).gt.diam(1)) then
	         b_mar(i,1)=log(g(i,1))-gam(i,1)
	      endif
	   write(36,120)r(i)-diam(1),beff_mar(i),b_mar(i,1)-beff_mar(i),
     c			beff_mar(i)-b_mar(i,1),b_mar(i,1)
	enddo
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	close(31)
	close(35)
	close(36)



c	mk=m(1)
c      a=(gameff(mk+1)-gameff(mk))/dr
c      b=gameff(mk)-a*r(mk)
c      gameffcont=a*diam(1)+b 
c      ceffcontn=-1.d0-gameffcont
c      ceffcontp=gcont(1)-1.d0-gameffcont
c      write(10,*)'ceff(sigma1-)=',sngl(ceffcontn)
c      write(10,*)'ceff(sigma1+)=',sngl(ceffcontp)     
c      write(15,300)0.d0,-ceffcontp,-dlog(gcont(1))
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc    
      do i=1,nr
         if (r(i).gt.diam(1)) then
            if (r(i).le.diam(1)+10.d0*diam(3)) then
               write(15,300)r(i)-diam(1),c2(i,1)-ceff(i)+beff(i)-b(i,1)
            endif
         endif
      enddo
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	open(37,file="df_py.dat",status="unknown")
      do i=1,nr
         if (r(i).gt.diam(1)) then
            if (r(i).le.diam(1)+10.d0*diam(3)) then
          write(37,100)r(i)-diam(1),c2(i,1)-ceff(i)+beff_py(i)-b_py(i,1)
            endif
         endif
      enddo
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	open(38,file="df_mar.dat",status="unknown")
      do i=1,nr
         if (r(i).gt.diam(1)) then
            if (r(i).le.diam(1)+10.d0*diam(3)) then
       write(38,100)r(i)-diam(1),c2(i,1)-ceff(i)+beff_mar(i)-b_mar(i,1)
            endif
         endif
      enddo

	close(37)
	close(38)


      return
100   format(4f15.4)
120   format(5f15.4)
200   format(2f15.4)      
300   format(3f15.4)
      end
c
c	This subroutine calculates the glass criterion
	subroutine glass(sq,dkmin)
	implicit integer*4(i-n),real*8(a-h,o-z)
	parameter (nm=2**17)
	dimension f(nm),sq(nm),f2(nm),gam(nm)
	dimension dl(nm)
	common/oz2c/q(nm),dr,kj
      common/sys/rho,phi,x(2),diam(3)
      common/par/alfa,rmax,ez,nrho,nr,iclos     
	pi=4.d0*datan(1.d0)
	dq=pi/rmax
	ngam=4000
c	open(72,file='fl.dat',status='unknown')
	do k=1,ngam
	   gam(k)=k*0.001d0
	   do i=2,nr
	      dl(i)=1./(1.+(q(i)/dkmin)**2.)
	      del=(dl(i)*sq(i)+q(i)**2*gam(k))*(dl(i)+q(i)**2*gam(k))
	      f(i)=((sq(i)-1.d0)*dl(i))**2.*q(i)**4.*gam(k)/del
	   enddo
	   prod=0.d0
         do i=3,nr-1
            prod=prod+f(i)
         enddo
	   prod=prod*dq
         prod=prod+(f(2)+f(nr))*dq/2.d0
	   f2(k)=2*prod/(9*pi*phi)
	enddo
	open(70,file='phi.dat',status='unknown')
	do i=1,ngam
	   write(70,666)gam(i)/4.d0,f2(i)
	enddo
	close(70)
c	close(72)
666	format(2f15.8)
661	format(4f15.8)
	return
	end


      subroutine ng(n,gam0,gam,c,r)
      implicit integer*4(i-n),real*8(a-h,o-z)      
      parameter (nm=2**17,ngrad=5)
      dimension gam0(nm,3),gam(nm,3),c(nm,3),r(nm)
      dimension g1(nm,3),d1(nm,3),g2(nm,3),d2(nm,3),g3(nm,3)
      dimension d3(nm,3),g4(nm,3),d4(nm,3),g5(nm,3),d5(nm,3)
      dimension g6(nm,3),d6(nm,3)
      dimension d01(nm,3),d02(nm,3),d03(nm,3),d04(nm,3),d05(nm,3)
      dimension d01d01(3),d01d02(3),d01d03(3),d01d04(3),d01d05(3)
      dimension d02d02(3),d02d03(3),d02d04(3),d02d05(3)
      dimension d03d03(3),d03d04(3),d03d05(3)
      dimension d04d04(3),d04d05(3)
      dimension d05d05(3)
      dimension d6d01(3),d6d02(3),d6d03(3),d6d04(3),d6d05(3)
      dimension dij(ngrad,ngrad),di(ngrad)
      dimension f(nm,3)
      common/oz2c/q(nm),dr,kj
      common/sys/rho,phi,x(2),diam(3)
      common/par/alfa,rmax,ez,nrho,nr,iclos           
      do k=1,3
         do i=1,n
            f(i,k)=gam0(i,k)
         enddo
      enddo 
      call ong(n,r,f,g1,c)
      do k=1,3
         do i=1,n
            d1(i,k)=g1(i,k)-f(i,k)
         enddo
      enddo
      call ong(n,r,g1,g2,c)
      do k=1,3
         do i=1,n
            d2(i,k)=g2(i,k)-g1(i,k)
         enddo
      enddo
      call ong(n,r,g2,g3,c)
      do k=1,3
         do i=1,n
            d3(i,k)=g3(i,k)-g2(i,k)
         enddo
      enddo
      call ong(n,r,g3,g4,c)
      do k=1,3
         do i=1,n
            d4(i,k)=g4(i,k)-g3(i,k)
         enddo
      enddo
      call ong(n,r,g4,g5,c)
      do k=1,3
         do i=1,n
            d5(i,k)=g5(i,k)-g4(i,k)
         enddo
      enddo
300   call ong(n,r,g5,g6,c)
      if (kj.lt.2) goto 200
      do k=1,3
         do i=1,n
            d6(i,k)=g6(i,k)-g5(i,k)
         enddo
      enddo
      do k=1,3
         do i=1,n
            d01(i,k)=d6(i,k)-d5(i,k)
            d02(i,k)=d6(i,k)-d4(i,k)
            d03(i,k)=d6(i,k)-d3(i,k)
            d04(i,k)=d6(i,k)-d2(i,k)
            d05(i,k)=d6(i,k)-d1(i,k)
         enddo
      enddo
      call pp(n,dr,d01,d01,d01d01)
      call pp(n,dr,d01,d02,d01d02)
      call pp(n,dr,d01,d03,d01d03)
      call pp(n,dr,d01,d04,d01d04)
      call pp(n,dr,d01,d05,d01d05)
      call pp(n,dr,d6,d01,d6d01)
      call pp(n,dr,d02,d02,d02d02)
      call pp(n,dr,d02,d03,d02d03)
      call pp(n,dr,d02,d04,d02d04)
      call pp(n,dr,d02,d05,d02d05)
      call pp(n,dr,d6,d02,d6d02)
      call pp(n,dr,d03,d03,d03d03)
      call pp(n,dr,d03,d04,d03d04)
      call pp(n,dr,d03,d05,d03d05)
      call pp(n,dr,d6,d03,d6d03)
      call pp(n,dr,d04,d04,d04d04)
      call pp(n,dr,d04,d05,d04d05)
      call pp(n,dr,d6,d04,d6d04)
      call pp(n,dr,d05,d05,d05d05)
      call pp(n,dr,d6,d05,d6d05)
      do k=1,3
         dij(1,1)=d01d01(k)
         dij(1,2)=d01d02(k)
         dij(1,3)=d01d03(k)
         dij(1,4)=d01d04(k)
         dij(1,5)=d01d05(k)
         dij(2,1)=d01d02(k)
         dij(2,2)=d02d02(k)
         dij(2,3)=d02d03(k)
         dij(2,4)=d02d04(k)
         dij(2,5)=d02d05(k)
         dij(3,1)=d01d03(k)
         dij(3,2)=d02d03(k)
         dij(3,3)=d03d03(k)
         dij(3,4)=d03d04(k)
         dij(3,5)=d03d05(k)
         dij(4,1)=d01d04(k)
         dij(4,2)=d02d04(k)
         dij(4,3)=d03d04(k)
         dij(4,4)=d04d04(k)
         dij(4,5)=d04d05(k)
         dij(5,1)=d01d05(k)
         dij(5,2)=d02d05(k)
         dij(5,3)=d03d05(k)
         dij(5,4)=d04d05(k)
         dij(5,5)=d05d05(k)
         di(1)=d6d01(k)
         di(2)=d6d02(k)
         di(3)=d6d03(k)
         di(4)=d6d04(k)
         di(5)=d6d05(k)
         call solve(dij,di)
         do i=1,n
         f(i,k)=(1.d0-di(1)-di(2)-di(3)-di(4)-di(5))*g6(i,k)
         f(i,k)=f(i,k)+di(1)*g5(i,k)+di(2)*g4(i,k)
         f(i,k)=f(i,k)+di(3)*g3(i,k)+di(4)*g2(i,k)+di(5)*g1(i,k)
         enddo
      enddo
      call ong(n,r,f,g6,c)
      do k=1,3
         do i=1,n
            d6(i,k)=g6(i,k)-f(i,k)
         enddo
      enddo
100   call pres(d6,dr,n,eta)
      if ((eta-ez).le.0.d0) goto 200
      do k=1,3
         do i=1,n
            g1(i,k)=g2(i,k)
            d1(i,k)=d2(i,k)
            g2(i,k)=g3(i,k)
            d2(i,k)=d3(i,k)
            g3(i,k)=g4(i,k)
            d3(i,k)=d4(i,k)
            g4(i,k)=g5(i,k)
            d4(i,k)=d5(i,k)
            g5(i,k)=g6(i,k)
            d5(i,k)=d6(i,k)
         enddo
      enddo
      goto 300
200   do k=1,3
         do i=1,n
            gam(i,k)=g6(i,k)
         enddo
      enddo
      write(*,*)kj,sngl(eta)
      return
      end
c 
      subroutine solve(a,b)
      implicit integer*4(i-n),real*8(a-h,o-z)            
      parameter (ngrad=5)      
      dimension a(ngrad,ngrad),b(ngrad),indx(ngrad)      
      call ludcmp(a,indx,d)
      call lubksb(a,indx,b)
      return
      end
c
      subroutine ludcmp(a,indx,d)
      implicit integer*4(i-n),real*8(a-h,o-z)            
      parameter (ngrad=5,n=ngrad,np=ngrad)
      parameter (nmax=500,tiny=1.d-20)      
      dimension a(np,np),indx(n),vv(nmax)
      d=1.d0
      do 12 i=1,n
        aamax=0.d0
        do 11 j=1,n
          if (dabs(a(i,j)).gt.aamax) aamax=dabs(a(i,j))
11      continue
        if (aamax.eq.0.d0) pause 'singular matrix in ludcmp'
        vv(i)=1.d0/aamax
12    continue
      do 19 j=1,n
        do 14 i=1,j-1
          sum=a(i,j)
          do 13 k=1,i-1
            sum=sum-a(i,k)*a(k,j)
13        continue
          a(i,j)=sum
14      continue
        aamax=0.d0
        do 16 i=j,n
          sum=a(i,j)
          do 15 k=1,j-1
            sum=sum-a(i,k)*a(k,j)
15        continue
          a(i,j)=sum
          dum=vv(i)*dabs(sum)
          if (dum.ge.aamax) then
            imax=i
            aamax=dum
          endif
16      continue
        if (j.ne.imax)then
          do 17 k=1,n
            dum=a(imax,k)
            a(imax,k)=a(j,k)
            a(j,k)=dum
17        continue
          d=-d
          vv(imax)=vv(j)
        endif
        indx(j)=imax
        if(a(j,j).eq.0.d0)a(j,j)=tiny
        if(j.ne.n)then
          dum=1.d0/a(j,j)
          do 18 i=j+1,n
            a(i,j)=a(i,j)*dum
18        continue
        endif
19    continue
      return
      end
c
      subroutine lubksb(a,indx,b)
      implicit integer*4(i-n),real*8(a-h,o-z)            
      parameter(ngrad=5,n=ngrad,np=ngrad)
      dimension a(np,np),b(n),indx(n)
      ii=0
      do 12 i=1,n
        ll=indx(i)
        sum=b(ll)
        b(ll)=b(i)
        if (ii.ne.0)then
          do 11 j=ii,i-1
            sum=sum-a(i,j)*b(j)
11        continue
        else if (sum.ne.0.d0) then
          ii=i
        endif
        b(i)=sum
12    continue
      do 14 i=n,1,-1
        sum=b(i)
        do 13 j=i+1,n
          sum=sum-a(i,j)*b(j)
13      continue
        b(i)=sum/a(i,i)
14    continue
      return
      end
c 
      subroutine pp(n,dr,f,g,prod)
      implicit integer*4(i-n),real*8(a-h,o-z)
      parameter (nm=2**17)
      dimension f(nm,3),g(nm,3),prod(3)
      do k=1,3
         prod(k)=0.d0
         do i=2,n-1
            prod(k)=prod(k)+f(i,k)*g(i,k)
         enddo
         prod(k)=(prod(k)+(f(1,k)*g(1,k)+f(n,k)*g(n,k))/2.d0)*dr
      enddo
      return
      end
c 
      subroutine ong(n,r,gam0,gam1,c1)
      implicit integer*4(i-n),real*8(a-h,o-z)
      parameter (nm=2**17)
      dimension r(nm),gam0(nm,3),gam1(nm,3),c1(nm,3)
      dimension a0(3),a1(3),a2(3)
      common/oz2c/q(nm),dr,kj
      common/sys/rho,phi,x(2),diam(3)
      common/par/alfa,rmax,ez,nrho,nr,iclos         
      call closrel(n,r,gam0,c1)
      call count(c1,r,n,a0,a1,a2)
      call fftm(c1,rmax,n,1)
      call discount(c1,q,n,a0,a1,a2)
      do i=1,n
         delta=(1.d0-rho*x(1)*c1(i,1))*(1.d0-rho*x(2)*c1(i,3))
         delta=delta-rho**2*x(1)*x(2)*c1(i,2)**2
         gam1(i,1)=(1.d0-rho*x(2)*c1(i,3))*c1(i,1)
         gam1(i,1)=(gam1(i,1)+rho*x(2)*c1(i,2)**2)/delta
         gam1(i,1)=gam1(i,1)-c1(i,1)
         gam1(i,2)=c1(i,2)/delta-c1(i,2)
         gam1(i,3)=(1.d0-rho*x(1)*c1(i,1))*c1(i,3)
         gam1(i,3)=(gam1(i,3)+rho*x(1)*c1(i,2)**2)/delta
         gam1(i,3)=gam1(i,3)-c1(i,3)
      enddo
      call fftm(gam1,rmax,n,2)
      call closrel(n,r,gam1,c1)
      return
      end
c 
      subroutine closrel(n,r,gam,c)
      implicit integer*4(i-n),real*8(a-h,o-z)
      parameter (nm=2**17)
      dimension r(nm),gam(nm,3),c(nm,3),b(nm,3)
      common/sys/rho,phi,x(2),diam(3)
      common/par/alfa,rmax,ez,nrho,nr,iclos   
      if (iclos.eq.1) then
         do k=1,3
            do i=1,n
               if (r(i).lt.diam(k)) then
                  c(i,k)=-gam(i,k)-1.d0
               else
                  c(i,k)=0.d0
               endif
            enddo
         enddo
         return
      endif
      do k=1,3
         do i=1,n
            if (r(i).lt.diam(k)) then
               c(i,k)=-gam(i,k)-1.d0
            else
               if (gam(i,k) .gt. 0.d0) then
			    b(i,k)=-0.5*(gam(i,k)**2)/(1.+0.8*gam(i,k))
		     else
			    b(i,k)=-0.5*(gam(i,k)**2)/(1.-0.8*gam(i,k))
		     endif
               c(i,k)=dexp(gam(i,k)+b(i,k))-gam(i,k)-1.d0
            endif
         enddo
      enddo
      return
      end
c 
      subroutine count(f,r,n,a0,a1,a2)
      implicit integer*4(i-n),real*8(a-h,o-z)      
      parameter (nm=2**17)
      dimension f(nm,3),r(nm),a0(3),a1(3),a2(3),m(3)
      common/sys/rho,phi,x(2),diam(3)      
      dr=r(2)-r(1)
      do k=1,3
         do i=1,n
            if (r(i).lt.diam(k)) then
               m(k)=i
            endif
         enddo
      enddo
      do k=1,3
      mk=m(k)
      fpd=-274.d0*f(mk+1,k)+600.d0*f(mk+2,k)-600.d0*f(mk+3,k)
      fpd=fpd+400.d0*f(mk+4,k)-150.d0*f(mk+5,k)+24.d0*f(mk+6,k)
      fpd=fpd/(120.d0*dr)
      fppd=225.d0*f(mk+1,k)-770.d0*f(mk+2,k)+1070.d0*f(mk+3,k)
      fppd=fppd-780.d0*f(mk+4,k)+305.d0*f(mk+5,k)-50.d0*f(mk+6,k)
      fppd=fppd/(60.d0*dr**2)
      fd=fpd*(diam(k)-r(mk+1))+f(mk+1,k)
      fpi=274.d0*f(mk,k)-600.d0*f(mk-1,k)+600.d0*f(mk-2,k)
      fpi=fpi-400.d0*f(mk-3,k)+150.d0*f(mk-4,k)-24.d0*f(mk-5,k)
      fpi=fpi/(120.d0*dr)
      fppi=225.d0*f(mk,k)-770.d0*f(mk-1,k)+1070.d0*f(mk-2,k)
      fppi=fppi-780.d0*f(mk-3,k)+305.d0*f(mk-4,k)-50.d0*f(mk-5,k)
      fppi=fppi/(60.d0*dr**2)
      fi=fpi*(diam(k)-r(mk))+f(mk,k)
      delf=fd-fi
      delfp=fpd-fpi
      delfpp=fppd-fppi
      a0(k)=delf-diam(k)*delfp+diam(k)**2*delfpp/2.d0
      a1(k)=delfp-diam(k)*delfpp
      a2(k)=delfpp/2.d0
      do i=1,mk
         f(i,k)=f(i,k)+a0(k)+a1(k)*r(i)+a2(k)*r(i)**2
      enddo
      enddo
      return
      end
c 
      subroutine discount(f,q,n,a0,a1,a2)
      implicit integer*4(i-n),real*8(a-h,o-z)      
      parameter (nm=2**17)
      dimension f(nm,3),q(nm),fa(nm,3),a0(3),a1(3),a2(3)
      common/sys/rho,phi,x(2),diam(3)          
      pi=4.d0*datan(1.d0)
      do k=1,3
      do i=1,n
         if (q(i).gt.0.d0) then
            part1=a0(k)+2.d0*diam(k)*a1(k)+3.d0*diam(k)**2*a2(k)
            part1=part1-6.d0*a2(k)/q(i)**2
            part1=part1*dsin(q(i)*diam(k))/q(i)**3
            part2=-diam(k)*a0(k)+2.d0*a1(k)/q(i)**2-diam(k)**2*a1(k)
            part2=part2+6.d0*diam(k)*a2(k)/q(i)**2
            part2=(part2-diam(k)**3*a2(k))*dcos(q(i)*diam(k))/q(i)**2
            part3=-2.d0*a1(k)/q(i)**4
            fa(i,k)=4.d0*pi*(part1+part2+part3)
            f(i,k)=f(i,k)-fa(i,k)
         else
            f(i,k)=0.d0
         endif
      enddo
      enddo
      return
      end
c
      subroutine extrap(gam0,gam,m,rho,drho)
      implicit integer*4(i-n),real*8(a-h,o-z)      
      parameter (nm=2**17)
      dimension gam0(nm,3),gam(nm,3)
      do k=1,3
         do i=1,m
            a=(gam(i,k)-gam0(i,k))/drho
            b=gam(i,k)-a*rho
            gam0(i,k)=a*(rho+drho)+b
         enddo
      enddo
      return
      end
C **************************
      SUBROUTINE PRES(F,DR,N,ETA)
      PARAMETER (nm=2**17)
      REAL*8 F,DR,ETA,AUX
      DIMENSION F(NM,3)
      ETA=0.D0
      DO 20 K=1,3
         AUX=0.D0
         DO 10 I=2,N-1
10          AUX=AUX+F(I,K)**2
         AUX=(AUX+(F(1,K)**2+F(N,K)**2)/2.D0)*DR
20       ETA =ETA+AUX
      ETA=DSQRT(ETA)
      RETURN
      END
C ********************************
      SUBROUTINE FFTM(FA,RMAX,N,II)
      IMPLICIT INTEGER*4(I-N), REAL*8(A-H,O-Z)      
      PARAMETER (nm=2**17)
      DIMENSION FA(NM,3),F(NM)
      DO 10 I=1,3
         DO 20 K=1,N
20          F(K)=FA(K,I)
         CALL FFT(F,RMAX,N,II)
         DO 30 K=1,N
30          FA(K,I)=F(K)
10    CONTINUE
      RETURN
      END
C **************************************
      SUBROUTINE FFT(F,RMAX,N,II)
      PARAMETER (nm=2**17)
C SI II=1 TF, SI II=2 TFI.
      REAL*8 F,A,RMAX
      DIMENSION F(NM)
      DO 10 I=1,N
         F(I)=(I-1)*F(I)
10    CONTINUE
      CALL SINFT(F,N,NM)
      IF (II.EQ.1) GOTO 20
      A=N*(1.D0/2.D0/RMAX**3)
      GOTO 30
20    A=4*RMAX**3/(1.D0*N**2)
      F(1)=0.D0
30    DO 40 I=2,N
      F(I)=A*F(I)/(1.D0*(I-1))
40    CONTINUE
      RETURN
      END
C *****************************
      SUBROUTINE SINFT(Y,N,NMAX)
      REAL*8 WR,WI,WPR,WPI,WTEMP,THETA,PI,Y
      DIMENSION Y(NMAX)
      PI=4*DATAN(1.D0)
      THETA=PI/DBLE(N)
      WR=1.0D0
      WI=0.0D0
      WPR=-2.0D0*DSIN(0.5D0*THETA)**2
      WPI=DSIN(THETA)
      Y(1)=0.D0
      M=N/2
      DO 11 J=1,M
      WTEMP=WR
      WR=WR*WPR-WI*WPI+WR
      WI=WI*WPR+WTEMP*WPI+WI
      Y1=WI*(Y(J+1)+Y(N-J+1))
      Y2=0.5*(Y(J+1)-Y(N-J+1))
      Y(J+1)=Y1+Y2
      Y(N-J+1)=Y1-Y2
11    CONTINUE
      CALL REALFT(Y,M,+1,NMAX)
      SUM=0.0
      Y(1)=0.5*Y(1)
      Y(2)=0.D0
      DO 12 J=1,N-1,2
      SUM=SUM+Y(J)
      Y(J)=Y(J+1)
      Y(J+1)=SUM
12    CONTINUE
      RETURN
      END
C ****************************************
      SUBROUTINE REALFT(DATA,N,ISIGN,NMAX)
      REAL*8 WR,WI,WPR,WPI,WTEMP,THETA,PI,DATA
      DIMENSION DATA(2*NMAX)
      PI=4*DATAN(1.D0)
      THETA=PI/DBLE(N)
      C1=0.5
      IF (ISIGN.EQ.1) THEN
      C2=-0.5
      CALL FOUR1(DATA,N,+1,NMAX)
      ELSE
      C2=0.5
      THETA=-THETA
      ENDIF
      WPR=-2.0D0*DSIN(0.5D0*THETA)**2
      WPI=DSIN(THETA)
      WR=1.0D0+WPR
      WI=WPI
      N2P3=2*N+3
c     DO 11 I=2,N/2+1
      do 11 i=2,n/2
      I1=2*I-1
      I2=I1+1
      I3=N2P3-I2
      I4=I3+1
      WRS=SNGL(WR)
      WIS=SNGL(WI)
      H1R=C1*(DATA(I1)+DATA(I3))
      H1I=C1*(DATA(I2)-DATA(I4))
      H2R=-C2*(DATA(I2)+DATA(I4))
      H2I=C2*(DATA(I1)-DATA(I3))
      DATA(I1)=H1R+WRS*H2R-WIS*H2I
      DATA(I2)=H1I+WRS*H2I+WIS*H2R
      DATA(I3)=H1R-WRS*H2R+WIS*H2I
      DATA(I4)=-H1I+WRS*H2I+WIS*H2R
      WTEMP=WR
      WR=WR*WPR-WI*WPI+WR
      WI=WI*WPR+WTEMP*WPI+WI
11    CONTINUE
      IF (ISIGN.EQ.1) THEN
      H1R=DATA(1)
      DATA(1)=H1R+DATA(2)
      DATA(2)=H1R-DATA(2)
      ELSE
      H1R=DATA(1)
      DATA(1)=C1*(H1R+DATA(2))
      DATA(2)=C1*(H1R-DATA(2))
      CALL FOUR1(DATA,N,-1,NMAX)
      ENDIF
      RETURN
      END
C ***************************************
      SUBROUTINE FOUR1(DATA,NN,ISIGN,NMAX)
      REAL*8 WR,WI,WPR,WPI,WTEMP,THETA,PI,DATA
      DIMENSION DATA(2*NMAX)
      PI=4*DATAN(1.D0)
      N=2*NN
      J=1
      DO 11 I=1,N,2
      IF (J.GT.I) THEN
      TEMPR=DATA(J)
      TEMPI=DATA(J+1)
      DATA(J)=DATA(I)
      DATA(J+1)=DATA(I+1)
      DATA(I)=TEMPR
      DATA(I+1)=TEMPI
      ENDIF
      M=N/2
1     IF ((M.GE.2).AND.(J.GT.M)) THEN
      J=J-M
      M=M/2
      GOTO 1
      ENDIF
      J=J+M
11    CONTINUE
      MMAX=2
2     IF (N.GT.MMAX) THEN
      ISTEP=2*MMAX
      THETA=2*PI/(ISIGN*MMAX)
      WPR=-2.D0*DSIN(0.5D0*THETA)**2
      WPI=DSIN(THETA)
      WR=1.D0
      WI=0.D0
      DO 13 M=1,MMAX,2
      DO 12 I=M,N,ISTEP
      J=I+MMAX
      TEMPR=SNGL(WR)*DATA(J)-SNGL(WI)*DATA(J+1)
      TEMPI=SNGL(WR)*DATA(J+1)+SNGL(WI)*DATA(J)
      DATA(J)=DATA(I)-TEMPR
      DATA(J+1)=DATA(I+1)-TEMPI
      DATA(I)=DATA(I)+TEMPR
      DATA(I+1)=DATA(I+1)+TEMPI
12    CONTINUE
      WTEMP=WR
      WR=WR*WPR-WI*WPI+WR
      WI=WI*WPR+WTEMP*WPI+WI
13    CONTINUE
      MMAX=ISTEP
      GOTO 2
      ENDIF
      RETURN
      END





